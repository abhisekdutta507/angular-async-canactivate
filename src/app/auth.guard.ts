import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';

import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private http: HttpService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    return this.Wait(state);
  }

  Wait(state: RouterStateSnapshot): Promise<boolean> {
    return new Promise(async (next, error) => {
      // console.log(state);

      let tR: any = await this.http.test().toPromise();

      /**@description check the conditions and proceed */

      // ... 

      // Sample Code Block

      /* if(tR.message.type === 'success') {
        next(true);
      } else {
        this.router.navigate(['/landing']);
        next(false);
      } */

      next(true); // use to allow to navigate to the route

      // ...

      // this.router.navigate(['/landing']); // use to go to your desired route

      // next(false); // use to block the navigation

      // ...
    });
  }
}
