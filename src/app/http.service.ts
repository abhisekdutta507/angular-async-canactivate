import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private apiurl = 'https://nodeapis101.herokuapp.com/api/v1';

  constructor(private http: HttpClient) { }

  /**
   * @description Returns HTTP Header Options.
   */
  option(type: any) {
    return {
      headers: new HttpHeaders(Object.assign({}, type, {}))
    };
  }

  /**
   * @description GET response from the server.
   */
  test(): Observable<HttpResponse<any>> {
    let url = `${this.apiurl}/test`;

    return this.http.get<any>(url);
  }
}
